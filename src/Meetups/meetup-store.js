import { writable } from "svelte/store";

const meetupStore = writable([
  {
    id: 'm1',
    title: 'title 1',
    subtitle: 'subtitle 1',
    desc: 'desc 1',
    imageUrl: '',
    address: 'ilsan1',
    email: 'hsm1529@gmail.com',
    isFavorite: false,
  },
  {
    id: 'm2',
    title: 'title 2',
    subtitle: 'subtitle 2',
    desc: 'desc 2',
    imageUrl: '',
    address: 'ilsan2',
    email: 'hsm1529@gmail.com',
    isFavorite: false,
  },
  {
    id: 'm3',
    title: 'title 3',
    subtitle: 'subtitle 3',
    desc: 'desc 3',
    imageUrl: '',
    address: 'ilsan3',
    email: 'hsm1529@gmail.com',
    isFavorite: false,
  }
]);


export default {
  subscribe: meetupStore.subscribe,
  add: (meetup) => {
    meetupStore.update((items) => {
      return [ 
        {
          ...meetup,
          id: Math.random().toString(),
          isFavorite: false,
        }, 
        ...items 
      ];
    });
  },
  updateMeetup: (id, meetupData) => {
    meetupStore.update((items) => {
      const meetupIndex = items.findIndex(i => i.id === id);
      const updatedMeetup = { ...items[meetupIndex], ...meetupData };
      const updatedMeetups = [ ...items ];
      updatedMeetups[meetupIndex] = updatedMeetup;
      return updatedMeetups;
    });
  },
  removeMeetup: (id) => {
    meetupStore.update(items => {
      return items.filter(i => i.id !== id);
    });
  },
  toggleFavorite: (id) => {
    meetupStore.update((items) => {
      const updatedMeetup = { ...items.find(item => item.id === id) };
      const index = items.findIndex(item => item.id === id);
      const updatedMeetups = [...items];
  
      updatedMeetup.isFavorite = !updatedMeetup.isFavorite;
      updatedMeetups[index] = updatedMeetup;
      return updatedMeetups;
    });
  },

};